from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .forms import *

# Create your views here.
def index(request):
    return render(request, 'index.html')

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        user_dict = {'username':username, 'password':password}
        if user is not None:
            login(request, user)
            return redirect('/')
        
        else:
            message = {'message':'Invalid username or password'}
            return render(request, 'login.html', message)
    
    else :
        return render(request, 'login.html')

def logout_user(request):
    request.session.flush()
    logout(request)
    return redirect('/')

def signup_user(request):
    if request.method == "POST":
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.profileImage = form.cleaned_data.get('image')
            user.save()
            return redirect('/login/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})